<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/edit/{id}','HomeController@edit')->name("empleado_edit");
Route::get('/home/show/{id}','HomeController@show')->name("empleado_show");

Route::put('/home/update/{id}','HomeController@update')->name("empleado_update");

Route::get('/home/activate/{id}','HomeController@activate')->name("empleado_activate");
Route::get('/home/deactivate/{id}','HomeController@deactivate')->name("empleado_deactivate");
Route::get('/home/delete/{id}','HomeController@delete')->name("empleado_delete");
