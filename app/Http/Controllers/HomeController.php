<?php

namespace App\Http\Controllers;
use App\Empleado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Validator;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function show($id){      
        $empleado = Empleado::find($id);
        return view('empleado.detalles', ["empleado"=>$empleado,"message"=>"editar"]);
    }

    public function edit($id){      
        $empleado = Empleado::find($id);
        return view('empleado.editar', ["empleado"=>$empleado,"message"=>"editar"]);
    }

    public function update($id){      
        $empleado = Empleado::find($id);
        $oldname= $empleado->nombre;
        $empleado->nombre = Input::get('name');
        $empleado->correo = Input::get('email');
        $empleado->save();
        return redirect("home")->with("status","$oldname Editado correctamente");
    }

    public function activate($id){      
        $empleado = Empleado::find($id);
        $empleado->activo = 1;
        $empleado->save();
        return redirect("home")->with("status","$empleado->nombre Activado correctamente");
    }

    public function deactivate($id){      
        $empleado = Empleado::find($id);
        $empleado->activo = 0;
        $empleado->save();
        return redirect("home")->with("status","$empleado->nombre Desactivado correctamente");
    }
    public function delete($id){      
        $empleado = Empleado::find($id);
        $empleado->eliminado = 1;
        $empleado->save();
        return redirect("home")->with("status","$empleado->nombre Eliminado correctamente");
    }

    public function index(){
        $empleados = Empleado::where("eliminado",0)->get();
        return view('home', ["empleados"=>$empleados,"message"=>"cargar"]);
    }
}
