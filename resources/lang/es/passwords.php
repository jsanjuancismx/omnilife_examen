<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La constraseña debe tener al menos 8 caracteres.',
    'reset' => 'Su contraseña ha sido restaurada!',
    'sent' => 'Le hemos enviado un enlace de recuperación a su correo!',
    'token' => 'El registro de validación no es válido.',
    'user' => "No existe un usuario con ese correo.",

];
