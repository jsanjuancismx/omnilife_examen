@extends('layouts.app')

@section('content')
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h4>Empleados</h4>
    <p class="lead">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger" role="alert">
                {{ session('error') }}
            </div>
        @endif
        <div class="container">
            <table class="table table-light table-striped table-hover table-md">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">codigo<th>
                        <th scope="col">nombre<th>
                        <th scope="col">salarioDolares<th>
                        <th scope="col">salarioPesos<th>
                        <th scope="col">correo<th>
                        <th scope="col">activo<th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($empleados as $key => $empleado)
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                        <td scope="col">{{$empleado->codigo}}<td>
                        <td scope="col">{{$empleado->nombre}}<td>
                        <td scope="col">{{$empleado->salarioDolares}}<td>
                        <td scope="col">{{$empleado->salarioPesos}}<td>
                        <td scope="col">{{$empleado->correo}}<td>
                        <td scope="col">
                            @if($empleado->activo == 1)
                            <i class="fa fa-check"></i>
                            @else
                            <i class="fa fa-uncheck"></i>
                            @endif
                        <td>
                        <td scope="col" style="display: flex">
                            <button title="Detalles" class="btn btn-light"><a href="{{route('empleado_show',$empleado->id)}}"><i class="fa fa-list"></i></a></button>
                            <button title="Editar" class="btn btn-light"><a href="{{route('empleado_edit',$empleado->id)}}"><i class="fa fa-pencil"></i></a></button>
                            @if($empleado->activo == 1)
                            <button title="Desactivar" class="btn btn-light"><a href="{{route('empleado_deactivate',$empleado->id)}}"><i class="fa fa-toggle-on" ></i></a></button>
                            @else
                            <button title="Desactivar" class="btn btn-light"><a href="{{route('empleado_activate',$empleado->id)}}"><i class="fa fa-toggle-off" ></i></a></button>
                            @endif
                            <button title="Eliminar" class="btn btn-light"><a href="{{route('empleado_delete',$empleado->id)}}"><i class="fa fa-trash"></i></a></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </p>
  </div>
</div>
@endsection
