@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verficar su dirección de correo.') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Se ha enviado un enlace de verificación a su correo.') }}
                        </div>
                    @endif

                    {{ __('Antes de continuar, verifique su dirección de correo.') }}
                    {{ __('Si no recibe el correo') }}, <a href="{{ route('verification.resend') }}">{{ __('clic aquí para reenviarlo.') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
