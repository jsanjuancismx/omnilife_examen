@extends('layouts.app')

@section('content')
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h4>Editar cédula de <strong><i>{{$empleado->nombre}}</i></strong></h4>
    <p class="lead">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="container">
          {{ Form::model($empleado, array('route' => array('empleado_update', $empleado->id), 'method' => 'PUT')) }}
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="edit_codigo">Código</label>
                <input type="text" class="form-control" id="edit_codigo" placeholder="Código" disabled value="{{$empleado->codigo}}">
              </div>
              <div class="form-group col-md-6">
                <label for="edit_nombre">Nombre</label>
                {{ Form::text('name', $empleado->nombre, ['class' => 'form-control', 'placeholder'=>$empleado->nombre]) }}
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="edit_codigo">Email</label>
                {{ Form::email('email', $empleado->correo, ['class' => 'form-control', 'placeholder'=>$empleado->correo]) }}
              </div>
              <div class="form-group col-md-6">
                <label for="edit_nombre">Situación</label>
                <select class="form-control" value="{{$empleado->activo}}">
                  <option value="1">Activo</option>
                  <option value="0">Inactivo</option>
                </select>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label class="sr2-only" for="edit_sueldousd">Salario en dólares</label>
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">USD</div>
                  </div>
                  <input type="text" class="form-control" id="edit_sueldousd" placeholder="Username" value="{{$empleado->salarioDolares}}">
                </div>
              </div>
              <div class="form-group col-md-6">
                <label class="sr2-only" for="edit_sueldomxn">Salario en pesos</label>
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">MXN</div>
                  </div>
                  <input type="text" class="form-control" id="edit_sueldomxn" placeholder="Username" value="{{$empleado->salarioPesos}}">
                </div>
              </div>
              
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <button class="btn btn-light" type="submit">Cancelar</button>
              </div>
              <div class="form-group col-md-6">
                <button class="btn btn-primary" type="submit">Guardar</button>
              </div>
              
            </div>
          {{ Form::close() }}
        </div>
    </p>
  </div>
</div>
@endsection
