@extends('layouts.app')

@section('content')
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <p class="lead">
        <h1>Detalle</h1>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="card">
          @if($empleado->activo == 1)
          <div class="card-header bg-success txt-default">
            <strong style="color:white"><i>{{$empleado->nombre}} (Activo)</i></strong>
          </div>
          @else
          <div class="card-header bg-danger txt-default">
            <strong style="color:white"><i>{{$empleado->nombre}} (Inactivo)</i></strong>
          </div>
          @endif
          
          <div class="card-body">
            <ul class="list-group">
              <li class="list-group-item"><i class="fa fa-envelope pull-left fa-2x"></i><span class="pull-right">{{$empleado->correo}}</span></li>
              <li class="list-group-item">USD.<i class="fa fa-receipt fa-2x"></i><span class="pull-right">{{$empleado->salarioDolares}}</span></li>
              <li class="list-group-item">MXN<i class="fa fa-file-invoice-dollar pull-left fa-2x"></i><span class="pull-right">{{$empleado->salarioPesos}}</span></li>
              
            </ul>
          </div>
        </div>
    </p>
  </div>
</div>
@endsection
