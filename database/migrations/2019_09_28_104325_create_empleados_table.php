<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("codigo",64);
            $table->string("nombre",128);
            $table->double("salarioDolares",16,2);
            $table->double("salarioPesos",16,2);
            $table->string("direccion",256);
            $table->string("estado",64);
            $table->string("ciudad",64);
            $table->string("telefono",16);
            $table->string("correo",128);
            $table->boolean("activo");
            $table->boolean("eliminado");
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
