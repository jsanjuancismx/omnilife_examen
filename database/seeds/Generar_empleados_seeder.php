<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class Generar_empleados_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
    	$faker = Faker::create();
		for ($i=0; $i < 50; $i++) {
			$salarioMxn= $faker->randomFloat($nbMaxDecimals= 2, $min=0, $max=100000.0);
			$salarioUsd= $salarioMxn/21;
		    \DB::table('empleados')->insert(array(
		    	"id"=>0,
		 		"codigo"=>$faker->bothify("E-###???"),
		 		"nombre"=>$faker->name(),
		 		"salarioDolares"=>$salarioUsd,
		 		"salarioPesos"=>$salarioMxn,
		 		"direccion"=>$faker->streetAddress,
		 		"estado"=>$faker->state,
		 		"ciudad"=>$faker->city,
		 		"telefono"=>$faker->e164PhoneNumber,
		 		"correo"=>$faker->freeEmail,
		 		"activo"=>$faker->randomElement([TRUE, FALSE]),
		 		"eliminado"=>$faker->randomElement([TRUE, FALSE]),
		 		"created_at"=>$faker->dateTime($timeZone = null)
		    ));
		}
    }
}
